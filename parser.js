"use strict";

const calc = function() {
    var sumLines = (arr) => {
        return Array.prototype.reduce.call(arr, (prev, curr) => {
            return prev + Number.parseInt(curr.innerText);
        }, 0);
    };
    return {
        'Pull request': document.getElementsByClassName("issue-title")[0].text,
        'Added': sumLines(document.getElementsByClassName("lines-added")),
        'Removed': sumLines(document.getElementsByClassName("lines-removed"))
    };
};

chrome.runtime.onMessage.addListener(function(req, sender, cb) {
    cb(calc());
});

window.addEventListener('load', function(event) {
    console.log(calc());
});
