"use strict";
chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    //document.getElementById('result').innerText = 5;
    chrome.tabs.sendMessage(tabs[0].id, {action: "calc"}, function(res) {
        var score = Number.parseInt(res.Added) + Number.parseInt(res.Removed);

        document.getElementById('added').innerText = "Added: " + res.Added;
        document.getElementById('removed').innerText = "Removed: " + res.Removed;
        document.getElementById('request').innerText = "Request: " + res["Pull request"];
        document.getElementById('score').innerText = "Scored: " + score;
    });
});
